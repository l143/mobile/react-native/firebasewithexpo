import 'dotenv/config';

export default {
  "name": "whiteapp",
  "slug": "whiteapp",
  "version": "1.0.0",
  "orientation": "portrait",
  "icon": "./assets/icon.png",
  "splash": {
    "image": "./assets/splash.png",
    "resizeMode": "contain",
    "backgroundColor": "#ffffff"
  },
  "updates": {
    "fallbackToCacheTimeout": 0
  },
  "assetBundlePatterns": [
    "**/*"
  ],
  "ios": {
    "supportsTablet": true
  },
  "android": {
    "adaptiveIcon": {
      "foregroundImage": "./assets/adaptive-icon.png",
      "backgroundColor": "#FFFFFF"
    },
    "package": "whiteapp.com"
  },
  "web": {
    "favicon": "./assets/favicon.png"
  },
  "plugins": [
    "@react-native-firebase/app"
  ],
  "extra": {
    "SomeEnvVar": process.env.SomeEnvVar,
  }
}
