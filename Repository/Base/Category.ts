import type { Category as CategoryEntity } from "../../Entities/Category"
export interface Category {
  getAll: () => Promise<Array<CategoryEntity>>
}