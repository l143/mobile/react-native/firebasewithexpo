import type { Category } from '../Base/Category'
import type { Category as CategoryEntity } from "../../Entities/Category"
import firestore from '@react-native-firebase/firestore';

const  collectionName = 'Categories'
export class CategoryRepo implements Category {


  getAll = async (): Promise<Array<CategoryEntity>> => {
    const res: CategoryEntity[] = []
    const data = await firestore().collection(collectionName).get()
    data.forEach((doc) => {
      const tmp: CategoryEntity = {
        ...doc.data() as CategoryEntity,
        id: doc.id
      }
      res.push(tmp)
    });
    return res

  }
}