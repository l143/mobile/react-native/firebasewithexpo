import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

import { useEffect, useState } from 'react'
import { Category } from "./Entities/Category"
import { CategoryRepo } from "./Repository/firebase/Category"


const repo = new CategoryRepo()

export default function App() {

  const [data, setData] = useState<Array<Category>>([])


  useEffect(() => {
    repo.getAll()
      .then(res => {
        setData(res)
      })
  }, [])

  return (
    <View style={styles.container}>
      <Text>Firebase!</Text>
      {
        data.map(el => {
          return <Text key={el.id} > {el.name} </Text>
        })
      }
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
