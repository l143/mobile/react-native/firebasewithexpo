export type Category = {
  id: string,
  name: string,
  items: Array<string>
}